# Paymo Authentication
api_key = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
password = '' #leave blank?

# Remote Storage
remote_user = ''
remote_pass = ''
remote_token = ''
remote_path = ''

# Local Storage (default is relative to where script is started from, no trailing slash)
local_path = 'backups'

# time (in seconds) to delay each API request
rate_limit = 1

# The following allows you to choose the method of retrieving endpoints.
# Default is 'includes', and condenses data minimizing the need to loop through more endpoints. 
# However this may fail if you have too much data in 1 endpoint. The 'single' method can then be used instead.
METHOD = 'includes'

# Endpoints that return full data
endpoints = [
    'clientcontacts',
    'clients',
    'comments',
    'company',
    'discussions',
    'estimatetemplates',
    'estimates',
    'expenses',
    'files',
    'invoicetemplates',
    'invoicepayments',
    'invoices',
    'milestones',
    'projecttemplates',
    'projectstatuses',
    'projects',
    'reports',
    'sessions',
    'tasklists',
    'taskrecurringprofiles',
    'tasks',
    'timeentries',
    'users',
    'taskassignments',
    'workflows',
    'workflowstatuses'
]

# Endpoints that require additional parameters
endpoints2 = [
    'bookings',
    'subtasks'
]