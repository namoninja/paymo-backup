# Paymo Backup

This project uses the official API to attempt to perform a full backup of data & offer restore options.

# Installation / How to use

```
git clone git@gitlab.com:namoninja/paymo-backup.git
cd paymo-backup
chmod +x paymo-backup.py
```
Generate an API Key from the Paymo application, on the My Account page.<BR>
edit `config.py` and add your api_token then run `python paymo-backup.py` (tested on python3.9)

# Automation

add cronjob to run `paymo-backup.py` <BR>
will add remote storage option later. <BR>

# Current Functionality

* Able to backup all raw text data in json
* Able to download invoices as pdfs
* Able to download other attachments & files
* Adheres to API rate limit (default set to 1s per request)
* If you would like to download only specific endpoints, you can comment out the ones you don't want in config.py
* Backups are stored in timebased folders
* Two methods of retrieving endpoint data

Note: If you have a lot of data, it may take a while due to the API rate limit and the loops.<BR>
Note: If you have too much data in an endpoint you will need to use the 'single' method.

# Features to add

* Proper naming/organization of attachments downloads
* Partial endpoint/range specified backups
* Partial Restore functionality
* Remote storage location

# Links

* Paymo App - https://www.paymoapp.com/
* Paymo API - https://github.com/paymoapp/api