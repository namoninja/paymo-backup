import requests
import time
import json
import logging
import sys
import argparse
import os, os.path
from datetime import datetime
from config import *


headers = {'Accept': 'application/json'}
time_start = datetime.now()
#time_now = time_start.strftime("%Y-%m-%d_%H%M%S")
time_now = time_start.strftime("%Y-%m-%d")
taskids = projectids = args = []

# Handles passing of arguments to script
def configure_args():
    global args
    all_args = argparse.ArgumentParser()

    # Add arguments to the parser
    all_args.add_argument(
        "-m",
        choices=['includes','single'],
        default='includes',
        required=False,
        help="This allows you to choose the method of retrieving endpoints. Default is includes, and condenses data minimizing the need to loop through more endpoints. However this may fail if you have too much data in 1 endpoint. The single method can then be used."
    )
    all_args.add_argument(
        "-e",
        required=False,
        choices=endpoints,
        default='all',
        help="Specify a comma seperated list of endpoints you would like to backup. Default is all."
    )
    args = vars(all_args.parse_args())

# Configures logger to record both to stdout and to file
def configure_logger():
    os.makedirs(os.path.dirname(f'{local_path}/{time_now}/debug.log'), exist_ok=True)
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s] %(message)s',
        handlers=[
            logging.FileHandler(f'{local_path}/{time_now}/debug.log'),
            logging.StreamHandler()
        ]
    )

# Save retrieved endpoint data to file
def save_data(endpoint,content,method,idx='',index='',total=''):
    try:
        with save_file(f'{local_path}/{time_now}/{endpoint}.json', method) as f:
            print(json.dumps(content), file=f)
        if '-inc' in endpoint:
            logging.info(f"({index}/{total}) {idx} saved to {endpoint}")
        else:
            logging.info(f"{endpoint} saved to file.")  
    except Exception as e:
        logging.error(f"Failed to save {endpoint} to file. ({e})")

# Loop through endpoints and retrieve data
def get_data():
    time_start = get_time('start_data')
    for endpoint in endpoints:
        time.sleep(rate_limit) #api rate limit

        # Collect primary endoint data
        r = requests.get(f'https://app.paymoapp.com/api/{endpoint}', headers=headers, auth=(api_key, password))
        save_data(endpoint, r.json(), 'w')

        if endpoint == 'tasks':
            # Collect task ids for secondary endpoints
            for task in r.json()['tasks']:
                taskids.append(task['id'])

            if METHOD == 'includes':
                # Collect primary endpoint data with includes
                time.sleep(rate_limit) #api rate limit
                r = requests.get(f'https://app.paymoapp.com/api/{endpoint}?include=subtasks,projects', headers=headers, auth=(api_key, password))
                save_data(f'{endpoint}-incl', r.json(), 'w')

        if endpoint == 'projects':
            # Collect project ids for secondary endpoints
            for project in r.json()['projects']:
                projectids.append(project['id'])
            
            if METHOD == 'includes':
                # Collect primary endpoint data with includes
                logging.info(f"Collecting data for {len(projectids)} projects...")
                i = 1 
                for projectid in projectids:
                    time.sleep(rate_limit) #api rate limit
                    try:
                        r = requests.get(f'https://app.paymoapp.com/api/{endpoint}/{projectid}?include=tasklists,tasklists.tasks,tasklists.tasks.subtasks', headers=headers, auth=(api_key, password))
                        save_data(f'{endpoint}-inc', r.json(), 'a', projectid, i, len(projectids))
                    except Exception as e:
                        logging.error(f'Failed to fetch endpoint. ({e})')
                    i += 1

    if METHOD != 'includes':
        # Loop through secondary endpoints
        for endpoint in endpoints2:
            logging.info(f'NOTE Started looping through {endpoint} for {len(taskids)} tasks.')
            for taskid in taskids:
                time.sleep(rate_limit) #api rate limit
                r = requests.get(f'https://app.paymoapp.com/api/{endpoint}?where=task_id={taskid}', headers=headers, auth=(api_key, password))
                # Check if there is any data and save to file
                if not r.json()[endpoint]:
                    logging.info(f'NOTE: Skipped taskid {taskid} for {endpoint} as it did not return any data.')
                else:
                    save_data('subtasks', '', r.json()[endpoint], 'a')

# Loop through endpoints and download attachments
def get_attachments(endpoint):

    # Read data from file
    try:
        with open(f'{local_path}/{time_now}/{endpoint}.json', 'r') as f:
            data = f.read()
        logging.info(f"Opened {endpoint}.json for getting attachment URLs.")
    except Exception as e:
        logging.error(f"ERROR: failed to open {endpoint}.json ({e})")
    jsondata = json.loads(data)

    # Fetch invoices as PDF
    if endpoint == 'invoices':
        total_items = len(jsondata['invoices'])
        n = 1

        logging.info(f"Starting download for {total_items} PDF invoices.")
        for i in range(total_items):
            url = jsondata['invoices'][i]['pdf_link']
            name = jsondata['invoices'][i]['number']
            
            try:
                logging.info(f"({n}/{total_items}) Downloading {name}.pdf...")
                dl = requests.get(url, allow_redirects=True)
                with save_file(f'{local_path}/{time_now}/attachments/{endpoint}/{name}.pdf', 'wb') as f:
                    f.write(dl.content)
            except Exception as e:
                logging.error(f'ERROR: Failed to download {name}. ({e})')
            n += 1
    
    # Fetch attachments
    elif endpoint == 'files':
        total_items = len(jsondata['files'])
        n = 1

        logging.info(f"Starting download for {total_items} attachments.")
        for i in range(total_items):
            filename = jsondata['files'][i]['original_filename']
            url = jsondata['files'][i]['download_url']
            task_id = jsondata['files'][i]['task_id']

            try:
                logging.info(f"({n}/{total_items}) Downloading {filename}...")
                dl = requests.get(url, allow_redirects=True)
                with save_file(f'{local_path}/{time_now}/attachments/{endpoint}/{task_id}-{filename}', 'wb') as f:
                    f.write(dl.content)
            except Exception as e:
                logging.error(f'ERROR: Failed to download {filename}. ({e})')
            n += 1

    else:
        pass

# This allows us to create any directories that are missing before writing file
def save_file(path, method):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    if method == 'wb':
        return open(path, 'wb')
    elif method == 'w':
        return open(path, 'w')
    elif method == 'a':
        return open(path, 'a')

# Compress backup folder to create archive.
def make_archive(folder):
    pass #not implemented

# Compress backup folder to create archive.
def push_remote(archive,location,user,passw,token):
    pass #not implemented

# Calculate how long the backup took
def get_time(activity):
    if activity == 'start':
        time_start = datetime.now()
        logging.info(f'SCRIPT STARTED at {time_start}')
    elif activity == 'start_data':
        time_start = datetime.now()
        logging.info(f'Started backing up data at {time_start}')
        return time_start
    elif activity == 'end_data':
        time_end = datetime.now()
        time_elapsed = time_end - time_start
        logging.info(f'Time Elapsed: {divmod(time_elapsed.seconds, 60)} (min, sec)')
    elif activity == 'start_attachments':
        time_start = datetime.now()
        logging.info(f'Started backing up attachments at {time_start}')
    elif activity == 'end_attachments':
        time_end = datetime.now()
        time_elapsed = time_end - time_start
        logging.info(f'Time Elapsed: {divmod(time_elapsed.seconds, 60)} (min, sec)')

if __name__ == '__main__':
    configure_args()
    configure_logger()
    get_data() # Get raw data dumps in .json format
    get_attachments('invoices') # Get invoices as PDF
    get_attachments('files') # Get other attachments
    #make_archive()
    #push_remote()
    #finish()